# A simple API to authorize and register users with Bearer and stuff
## Variables

Variables in Gitlab CI/CD:
```
QODANA_TOKEN - if you are using qodana, you can add (you should also uncomment the qodana field in gitlab-ci.yml)
DOCKERHUB_USER - docker hub user
DOCKERHUB_PASSWORD - docker hub password api key
NUGET_API_KEY - package api key (nuget.org > profile > api key)
DEPLOY_HOOK - deploy hook
```
```
PACKAGE_VERSION - version for nuget package (can be set manually, 
or use different variables to define new pipelines as new package version, 
for example: for gitlab - CI_PIPELINE_IID, for bitbucket - BITBUCKET_BUILD_NUMBER).
```
Locally (environment):
```
DeployConnection - database connection string
Key - unique key for the bearer (for example: Guid.NewGuid())
```
***

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details
