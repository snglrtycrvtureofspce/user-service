﻿using snglrtycrvtureofspce.Core.Base.Responses;
using snglrtycrvtureofspce.User.Application.DTOs;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RefreshToken;

public class RefreshTokenResponse : ItemResponse<TokenInfo>;