﻿using System.IdentityModel.Tokens.Jwt;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Application.Extensions;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Errors;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RefreshToken;

public class RefreshTokenHandler(UserManager<UserEntity> userManager, IConfiguration configuration)
    : IRequestHandler<RefreshTokenRequest, RefreshTokenResponse>
{
    public async Task<RefreshTokenResponse> Handle(RefreshTokenRequest request, CancellationToken cancellationToken)
    {
        var principal = JwtBearerExtensions.GetPrincipalFromExpiredToken(request.AccessToken);
        if (principal == null)
        {
            throw new ValidationException(TokenErrors.InvalidAccessToken());
        }
        
        var userIdClaim = principal.FindFirst("UserId");
        if (userIdClaim == null)
        {
            throw new ValidationException(TokenErrors.InvalidAccessToken());
        }

        var user = await userManager.FindByIdAsync(userIdClaim.Value);
        if (user == null)
        {
            throw new NotFoundException(UserErrors.NotFound());
        }
        
        if (user.RefreshToken != request.RefreshToken || user.RefreshTokenExpiryTime <= DateTime.UtcNow)
        {
            throw new ValidationException(TokenErrors.InvalidRefreshToken());
        }

        var roles = (await userManager.GetRolesAsync(user))
            .Select(roleName => new IdentityRole<Guid> { Name = roleName })
            .ToList();
        var authClaims = user.CreateClaims(roles);
        var accessToken = configuration.CreateToken(authClaims);

        var refreshToken = JwtBearerExtensions.GenerateRefreshToken();
        
        user.RefreshToken = refreshToken;
        user.RefreshTokenExpiryTime = DateTime.UtcNow.AddDays
            (configuration.GetValue<int>("Jwt:RefreshTokenValidityInDays"));
        await userManager.UpdateAsync(user);

        var model = new TokenInfo
        {
            AccessToken = new JwtSecurityTokenHandler().WriteToken(accessToken),
            RefreshToken = user.RefreshToken
        };

        var response = new RefreshTokenResponse
        {
            Message = "Tokens have been successfully refreshed.",
            Item = model
        };

        return response;
    }
}