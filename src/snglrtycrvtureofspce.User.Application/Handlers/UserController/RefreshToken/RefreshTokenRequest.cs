﻿using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RefreshToken;

public class RefreshTokenRequest : IRequest<RefreshTokenResponse>
{
    public string AccessToken { get; set; }
    
    public string RefreshToken { get; set; }
}