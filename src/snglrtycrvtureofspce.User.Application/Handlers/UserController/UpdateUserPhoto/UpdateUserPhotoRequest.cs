﻿using MediatR;
using Microsoft.AspNetCore.Http;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.UpdateUserPhoto;

public class UpdateUserPhotoRequest : IRequest<UpdateUserPhotoResponse>
{
    public Guid Id { get; set; }
    
    public IFormFile? UserPhoto { get; set; }
}