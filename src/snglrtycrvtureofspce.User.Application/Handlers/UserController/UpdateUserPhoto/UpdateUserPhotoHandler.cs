﻿using AutoMapper;
using MediatR;
using Refit;
using snglrtycrvtureofspce.Api.File;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Domain.Errors;
using snglrtycrvtureofspce.User.Domain.Interfaces;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.UpdateUserPhoto;

public class UpdateUserPhotoHandler(IUserRepository repository, ISystemApi systemApi, IMapperBase mapper) : 
    IRequestHandler<UpdateUserPhotoRequest, UpdateUserPhotoResponse>
{
    public async Task<UpdateUserPhotoResponse> Handle(UpdateUserPhotoRequest request, 
        CancellationToken cancellationToken)
    {
        var user = await repository.GetUserAsync(request.Id) 
                   ?? throw new NotFoundException(UserErrors.NotFound(request.Id));
        
        string userPhoto = null;
        
        if (request.UserPhoto != null)
        {
            await using var photoStream = request.UserPhoto.OpenReadStream();
            var photoStreamPart = new StreamPart(photoStream, request.UserPhoto.FileName, request.UserPhoto.ContentType);
            
            var uploadResponse = await systemApi.UploadSystemFilesAsync(photoStreamPart, 
                cancellationToken: cancellationToken);
            userPhoto = uploadResponse.Content.Item.FirstOrDefault()?.Url;
        }
        
        user.UserPhoto = userPhoto;
        
        await repository.UpdateUserAsync(user, cancellationToken);
        
        var model = mapper.Map<UserDto>(user);
        
        var response = new UpdateUserPhotoResponse
        {
            Message = "User photo have been successfully updated.",
            Item = model
        };
        
        return response;
    }
}