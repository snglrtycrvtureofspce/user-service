﻿using snglrtycrvtureofspce.Core.Base.Responses;
using snglrtycrvtureofspce.User.Application.DTOs;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.UpdateUserPhoto;

public class UpdateUserPhotoResponse : ItemResponse<UserDto>;