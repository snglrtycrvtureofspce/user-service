﻿using snglrtycrvtureofspce.Core.Base.Responses;
using snglrtycrvtureofspce.User.Application.DTOs;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.ConfirmPhoneNumber;

public class ConfirmPhoneNumberResponse : ItemResponse<UserDto>;