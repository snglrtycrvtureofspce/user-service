﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Errors;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.ConfirmPhoneNumber;

public class ConfirmPhoneNumberHandler(UserManager<UserEntity> userManager, IMapperBase mapper) 
    : IRequestHandler<ConfirmPhoneNumberRequest, ConfirmPhoneNumberResponse>
{
    public async Task<ConfirmPhoneNumberResponse> Handle(ConfirmPhoneNumberRequest request, 
        CancellationToken cancellationToken)
    {
        var user = await userManager.FindByIdAsync(request.UserId.ToString())
                   ?? throw new NotFoundException(UserErrors.NotFound(request.UserId));

        // Logic for verifying the confirmation code (for the future)
        // For example:
        // var result = await _userManager.VerifyChangePhoneNumberTokenAsync(user, request.Code, request.PhoneNumber);
        // if (!result)
        // {
        //     throw new Exception("Invalid code");
        // }

        user.PhoneNumberConfirmed = true;
        await userManager.UpdateAsync(user);

        var model = mapper.Map<UserDto>(user);

        var response = new ConfirmPhoneNumberResponse
        {
            Message = "The user's phone number was successfully confirmed.",
            Item = model
        };

        return response;
    }
}