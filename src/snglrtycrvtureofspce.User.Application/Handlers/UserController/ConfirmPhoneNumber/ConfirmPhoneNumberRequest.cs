﻿using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.ConfirmPhoneNumber;

public class ConfirmPhoneNumberRequest : IRequest<ConfirmPhoneNumberResponse>
{
    public Guid UserId { get; set; }
    
    public string PhoneNumber { get; set; }
    
    public string Code { get; set; }
}