﻿using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.DeleteUser;

public class DeleteUserRequest : IRequest<DeleteUserResponse>
{
    public Guid Id { get; init; }
}