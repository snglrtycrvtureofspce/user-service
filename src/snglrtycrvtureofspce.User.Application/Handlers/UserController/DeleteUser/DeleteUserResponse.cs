﻿using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.DeleteUser;

public class DeleteUserResponse : DeleteResponse;