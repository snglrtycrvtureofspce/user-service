﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.Core.Middlewares;
using snglrtycrvtureofspce.User.Domain.Errors;
using snglrtycrvtureofspce.User.Domain.Interfaces;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.DeleteUser;

public class DeleteUserHandler(IUserRepository repository) : IRequestHandler<DeleteUserRequest, DeleteUserResponse>
{
    public async Task<DeleteUserResponse> Handle(DeleteUserRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await repository.GetUserAsync(request.Id) 
                          ?? throw new NotFoundException(UserErrors.NotFound(request.Id));
            
            await repository.DeleteUserAsync(user, cancellationToken);
            
            var response = new DeleteUserResponse
            {
                Message = "User have been successfully deleted.",
                Id = request.Id
            };
            
            return response;
        }
        catch (DbUpdateException ex) 
            when (IsForeignKeyViolationExceptionMiddleware.CheckForeignKeyViolation(ex, out var referencedObject))
        {
            return new DeleteUserResponse
            {
                Message = $"Unable to delete user. It is referenced by {referencedObject}.",
                Id = request.Id
            };
        }
    }
}