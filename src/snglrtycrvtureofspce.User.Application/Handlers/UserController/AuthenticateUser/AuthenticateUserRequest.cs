﻿using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.AuthenticateUser;

public class AuthenticateUserRequest : IRequest<AuthenticateUserResponse>
{
    public string Email { get; init; }
    
    public string Password { get; init; }
}