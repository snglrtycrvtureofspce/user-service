﻿using snglrtycrvtureofspce.Core.Base.Responses;
using AuthResponse = snglrtycrvtureofspce.User.Application.DTOs.AuthResponse;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.AuthenticateUser;

public class AuthenticateUserResponse : ItemResponse<AuthResponse>;