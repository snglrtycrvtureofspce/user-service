﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Application.Extensions;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Errors;
using snglrtycrvtureofspce.User.Domain.Interfaces;
using snglrtycrvtureofspce.User.Domain.Services;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.AuthenticateUser;

public class AuthenticateUserHandler(UserManager<UserEntity> userManager, IUserRepository repository, 
    ITokenService tokenService, IConfiguration configuration) : 
    IRequestHandler<AuthenticateUserRequest, AuthenticateUserResponse>
{
    public async Task<AuthenticateUserResponse> Handle(AuthenticateUserRequest request, 
        CancellationToken cancellationToken)
    {
        var managedUser = await userManager
            .FindByEmailAsync(request.Email);
        
        if (managedUser == null)
        {
            throw new NotFoundException(UserErrors.NotFoundByEmail(request.Email));
        }
        
        var isPasswordValid = await userManager.CheckPasswordAsync(managedUser, request.Password);

        if (!isPasswordValid)
        {
            throw new ValidationException(UserErrors.PasswordIsInvalid());
        }
        
        var user = await repository.GetUserByEmailAsync(request.Email);
        
        if (user is null)
        {
            return new AuthenticateUserResponse
            {
                Message = "Password is invalid."
            };
        }
        
        var roleIds = await repository.GetUserRolesAsync(user.Id);
        var roles = await repository.GetRolesByIdsAsync(roleIds);
        
        var accessToken = tokenService.CreateToken(user, roles);
        user.RefreshToken = JwtBearerExtensions.GenerateRefreshToken();
        user.RefreshTokenExpiryTime = DateTime.UtcNow.AddDays(configuration
            .GetSection("Jwt:RefreshTokenValidityInDays").Get<int>());
        
        await repository.UpdateUserAsync(user, cancellationToken);
        
        var authResponse = new AuthResponse
        {
            Username = user.UserName,
            Email = user.Email,
            UserId = user.Id,
            AccessToken = accessToken,
            RefreshToken = user.RefreshToken
        };
        
        return new AuthenticateUserResponse
        {
            Message = "Authentication have been successful.",
            Item = authResponse
        };
    }
}