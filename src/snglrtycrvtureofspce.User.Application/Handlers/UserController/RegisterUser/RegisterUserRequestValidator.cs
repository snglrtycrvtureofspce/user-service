﻿using FluentValidation;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RegisterUser;

public class RegisterUserRequestValidator : AbstractValidator<RegisterUserRequest>
{
    public RegisterUserRequestValidator()
    {
        RuleFor(x => x.UserName)
            .NotEmpty().WithMessage("Username cannot be empty")
            .NotEmpty().WithMessage("Username cannot be null")
            .MaximumLength(20).WithMessage("Username cannot be longer than 20 characters")
            .Matches("^[a-zA-Z0-9]*$").WithMessage("Username cannot contain special characters");
        
        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Email cannot be empty")
            .NotEmpty().WithMessage("Email cannot be null")
            .Matches(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$")
            .WithMessage("Email must be a valid email address");
        
        RuleFor(x => x.FirstName)
            .NotEmpty().WithMessage("FirstName cannot be empty")
            .NotEmpty().WithMessage("FirstName cannot be null")
            .Matches("^[a-zA-Zа-яА-Я]*$").WithMessage("FirstName cannot contain numbers or special characters");
        
        RuleFor(x => x.LastName)
            .NotEmpty().WithMessage("LastName cannot be empty")
            .NotEmpty().WithMessage("LastName cannot be null")
            .Matches("^[a-zA-Zа-яА-Я]*$").WithMessage("LastName cannot contain numbers or special characters");
        
        RuleFor(x => x.DateOfBirth)
            .NotEmpty().WithMessage("DateOfBirth cannot be empty")
            .NotEmpty().WithMessage("DateOfBirth cannot be null")
            .Must(BeAValidDate).WithMessage("Date of birth must be valid");
        
        RuleFor(x => x.Agreement)
            .NotEmpty().WithMessage("Agreement cannot be empty")
            .NotEmpty().WithMessage("Agreement cannot be null");
        
        RuleFor(x => x.Password)
            .NotEmpty().WithMessage("Password cannot be empty")
            .NotEmpty().WithMessage("Password cannot be null")
            .MinimumLength(6).WithMessage("The password must contain at least 6 characters")
            .Must(BeAValidPassword).WithMessage("The password must contain at least one uppercase letter, " +
                                                "one lowercase letter, one digit, and one special character.");
        
        RuleFor(x => x.PasswordConfirm)
            .NotEmpty().WithMessage("The password for confirmation cannot be empty")
            .NotEmpty().WithMessage("The password for confirmation cannot be null")
            .Equal(x => x.Password).WithMessage("Password doesn't match");
    }
    
    private static bool BeAValidDate(DateTime? date)
    {
        return date.HasValue && date.Value < DateTime.UtcNow;
    }
    
    private static bool BeAValidPassword(string password)
    {
        if (string.IsNullOrWhiteSpace(password))
        {
            return false;
        }
        
        return password.Any(char.IsUpper) 
               && password.Any(char.IsLower) 
               && password.Any(char.IsDigit) 
               && password.Any(IsSpecialCharacter);
    }

    private static bool IsSpecialCharacter(char c) => !char.IsLetterOrDigit(c);
}