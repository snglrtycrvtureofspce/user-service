﻿using MediatR;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.AuthenticateUser;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RegisterUser;

public class RegisterUserRequest : IRequest<AuthenticateUserResponse>
{
    public string UserName { get; set; }
    
    public string Email { get; set; }
    
    public string FirstName { get; set; }
    
    public string LastName { get; set; }
    
    public string? MiddleName { get; set; }
    
    public DateTime? DateOfBirth { get; set; }
    
    public string? Country { get; set; }
    
    public string? City { get; set; }
    
    public bool Agreement { get; set; }
    
    public string Password { get; set; }
    
    public string PasswordConfirm { get; set; }
}