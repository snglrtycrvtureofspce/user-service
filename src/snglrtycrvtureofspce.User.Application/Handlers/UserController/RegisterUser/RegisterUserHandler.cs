﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.Core.Microservices.Core.JwtAuth.Entities;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.AuthenticateUser;
using snglrtycrvtureofspce.User.Domain.Errors;
using snglrtycrvtureofspce.User.Domain.Interfaces;
using UserEntity = snglrtycrvtureofspce.User.Domain.Entities.UserEntity;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RegisterUser;

public class RegisterUserHandler(UserManager<UserEntity> userManager, IUserRepository repository, ISender sender) : 
    IRequestHandler<RegisterUserRequest, AuthenticateUserResponse>
{
    public async Task<AuthenticateUserResponse> Handle(RegisterUserRequest request, CancellationToken cancellationToken)
    {
        var user = new UserEntity
        {
            UserName = request.UserName,
            Email = request.Email, 
            FirstName = request.FirstName, 
            LastName = request.LastName,
            MiddleName = request.MiddleName,
            DateOfBirth = request.DateOfBirth,
            Country = request.Country,
            City = request.City,
            IsActive = true,
            Agreement = request.Agreement
        };
            
        var result = await userManager.CreateAsync(user, request.Password);

        if (!result.Succeeded)
        {
            var errors = result.Errors.Select(error => error.Description);
            var errorMessage = string.Join(Environment.NewLine, errors);
            throw new ValidationException(errorMessage);
        }

        var findUser = await repository.GetUserByEmailAsync(request.Email) 
                       ?? throw new NotFoundException(UserErrors.NotFoundByEmail(request.Email));

        await userManager.AddToRoleAsync(findUser, RoleType.Member);
        
        var authRequest = new AuthenticateUserRequest
        {
            Email = request.Email,
            Password = request.Password
        };
        
        return await sender.Send(authRequest, cancellationToken);
    }
}