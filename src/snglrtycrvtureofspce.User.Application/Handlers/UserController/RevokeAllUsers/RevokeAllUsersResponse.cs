﻿using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeAllUsers;

public class RevokeAllUsersResponse : ItemResponse<string>;