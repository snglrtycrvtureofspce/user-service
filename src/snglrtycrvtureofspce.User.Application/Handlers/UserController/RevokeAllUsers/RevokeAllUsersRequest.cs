﻿using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeAllUsers;

public class RevokeAllUsersRequest : IRequest<RevokeAllUsersResponse>;