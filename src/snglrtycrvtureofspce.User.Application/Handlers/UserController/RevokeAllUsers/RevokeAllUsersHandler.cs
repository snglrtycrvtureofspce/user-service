﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeAllUsers;

public class RevokeAllUsersHandler(UserManager<UserEntity> userManager) : IRequestHandler<RevokeAllUsersRequest, 
    RevokeAllUsersResponse>
{
    public async Task<RevokeAllUsersResponse> Handle(RevokeAllUsersRequest request, CancellationToken cancellationToken)
    {
        var users = await userManager.Users.ToListAsync(cancellationToken: cancellationToken);
        
        foreach (var user in users)
        {
            user.RefreshToken = null;
            await userManager.UpdateAsync(user);
        }
        
        var response = new RevokeAllUsersResponse
        {
            Message = "All users have been successfully revoked."
        };
        
        return response;
    }
}