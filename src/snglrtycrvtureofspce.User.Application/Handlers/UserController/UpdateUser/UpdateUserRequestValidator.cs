using FluentValidation;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.UpdateUser;

public class UpdateUserRequestValidator : AbstractValidator<UpdateUserRequest>
{
    public UpdateUserRequestValidator()
    {
        RuleFor(x => x.UserName)
            .NotEmpty().WithMessage("Username cannot be empty")
            .NotEmpty().WithMessage("Username cannot be null")
            .MaximumLength(20).WithMessage("Username cannot be longer than 20 characters")
            .Matches("^[a-zA-Z0-9]*$").WithMessage("Username cannot contain special characters");
        
        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Email cannot be empty")
            .NotEmpty().WithMessage("Email cannot be null")
            .Matches(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$")
            .WithMessage("Email must be a valid email address");
        
        RuleFor(x => x.FirstName)
            .NotEmpty().WithMessage("FirstName cannot be empty")
            .NotEmpty().WithMessage("FirstName cannot be null")
            .Matches("^[a-zA-Zа-яА-Я]*$").WithMessage("FirstName cannot contain numbers or special characters");
        
        RuleFor(x => x.LastName)
            .NotEmpty().WithMessage("LastName cannot be empty")
            .NotEmpty().WithMessage("LastName cannot be null")
            .Matches("^[a-zA-Zа-яА-Я]*$").WithMessage("LastName cannot contain numbers or special characters");
        
        RuleFor(x => x.DateOfBirth)
            .NotEmpty().WithMessage("DateOfBirth cannot be empty")
            .NotEmpty().WithMessage("DateOfBirth cannot be null")
            .Must(BeAValidDate).WithMessage("Date of birth must be valid");
    }
    
    private static bool BeAValidDate(DateTime? date)
    {
        return date.HasValue && date.Value < DateTime.UtcNow;
    }
}