using MediatR;
using snglrtycrvtureofspce.User.Application.DTOs;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.UpdateUser;

public class UpdateUserRequest : IRequest<UpdateUserResponse>
{
    public Guid Id { get; set; }
    
    public string FirstName { get; set; }
    
    public string LastName { get; set; }
    
    public string? MiddleName { get; set; }
    
    public DateTime? DateOfBirth { get; set; }
    
    public string? Country { get; set; }
    
    public string? Email { get; set; }
    
    public string? UserName { get; set; }
    
    public string? City { get; set; }
    
    public string? Language { get; set; }
    
    public UserSocialDto UserSocial { get; set; }
    
    public List<UserAddressDto> UserAddresses { get; set; }
}