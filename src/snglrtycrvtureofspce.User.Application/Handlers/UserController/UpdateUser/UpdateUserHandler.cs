using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Errors;
using snglrtycrvtureofspce.User.Domain.Interfaces;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.UpdateUser;

public class UpdateUserHandler(UserManager<UserEntity> userManager, IUserRepository userRepository, 
    IUserAddressRepository userAddressRepository, IUserSocialRepository userSocialRepository, IMapperBase mapper) : 
    IRequestHandler<UpdateUserRequest, UpdateUserResponse>
{
    public async Task<UpdateUserResponse> Handle(UpdateUserRequest request, 
        CancellationToken cancellationToken)
    {
        var user = await userRepository.GetUserAsync(request.Id) 
                   ?? throw new NotFoundException(UserErrors.NotFound(request.Id));
        
        user.FirstName = request.FirstName;
        user.LastName = request.LastName;
        user.MiddleName = request.MiddleName;
        user.DateOfBirth = request.DateOfBirth;
        user.Country = request.Country;
        user.City = request.City;
        user.Language = request.Language;
        
        if (!string.IsNullOrWhiteSpace(request.UserName))
        {
            var normalizedUserName = userManager.NormalizeName(request.UserName);
            var existingUserWithUserName = await userRepository.GetUserByUserNameAsync(normalizedUserName);

            if (existingUserWithUserName != null && existingUserWithUserName.Id != user.Id)
            {
                throw new ValidationException(UserErrors.UsernameIsAlreadyInUse());
            }

            user.UserName = request.UserName;
            user.NormalizedUserName = normalizedUserName;
        }
        
        if (!string.IsNullOrEmpty(request.Email) && !string.Equals(user.Email, request.Email, StringComparison.OrdinalIgnoreCase))
        {
            var normalizedEmail = userManager.NormalizeEmail(request.Email);
            var existingUserWithEmail = await userRepository.GetUserByEmailAsync(normalizedEmail);

            if (existingUserWithEmail != null && existingUserWithEmail.Id != user.Id)
            {
                throw new ValidationException(UserErrors.EmailIsAlreadyInUse());
            }

            user.Email = request.Email;
            user.NormalizedEmail = normalizedEmail;
        }
        
        if (request.UserSocial != null)
        {
            var existingUserSocial = user.UserSocial ?? await userSocialRepository.GetUserSocialAsync(user.UserSocialId);

            if (existingUserSocial == null)
            {
                user.UserSocial = new UserSocialEntity
                {
                    Id = Guid.NewGuid(),
                    Site = request.UserSocial.Site,
                    FacebookLink = request.UserSocial.FacebookLink,
                    TwitterLink = request.UserSocial.TwitterLink,
                    InstagramLink = request.UserSocial.InstagramLink,
                    SkypeLink = request.UserSocial.SkypeLink,
                    LinkedIn = request.UserSocial.LinkedIn,
                    VkLink = request.UserSocial.VkLink,
                };
                
                await userSocialRepository.CreateUserSocialAsync(user.UserSocial, cancellationToken);
                user.UserSocialId = user.UserSocial.Id;
            }
            else
            {
                existingUserSocial.Site = request.UserSocial.Site;
                existingUserSocial.FacebookLink = request.UserSocial.FacebookLink;
                existingUserSocial.TwitterLink = request.UserSocial.TwitterLink;
                existingUserSocial.InstagramLink = request.UserSocial.InstagramLink;
                existingUserSocial.SkypeLink = request.UserSocial.SkypeLink;
                existingUserSocial.LinkedIn = request.UserSocial.LinkedIn;
                existingUserSocial.VkLink = request.UserSocial.VkLink;
                
                await userSocialRepository.UpdateUserSocialAsync(existingUserSocial, cancellationToken);
            }
        }
        
        if (request.UserAddresses != null && request.UserAddresses.Count > 0)
        {
            foreach (var addressViewModel in request.UserAddresses)
            {
                var existingAddress = await userAddressRepository.GetUserAddressByTypeAsync(user.Id, addressViewModel.AddressType);

                if (existingAddress != null)
                {
                    existingAddress.Country = addressViewModel.Country;
                    existingAddress.City = addressViewModel.City;
                    existingAddress.Street1 = addressViewModel.Street1;
                    existingAddress.Street2 = addressViewModel.Street2;
                    existingAddress.State = addressViewModel.State;
                    existingAddress.PostalCode = addressViewModel.PostalCode;
                    existingAddress.ModificationDate = DateTime.UtcNow;

                    await userAddressRepository.UpdateUserAddressAsync(existingAddress, cancellationToken);
                }
                else
                {
                    var newAddress = new UserAddressEntity
                    {
                        Id = Guid.NewGuid(),
                        Country = addressViewModel.Country,
                        City = addressViewModel.City,
                        Street1 = addressViewModel.Street1,
                        Street2 = addressViewModel.Street2,
                        State = addressViewModel.State,
                        PostalCode = addressViewModel.PostalCode,
                        AddressType = addressViewModel.AddressType,
                        UserId = user.Id,
                        CreatedDate = DateTime.UtcNow,
                        ModificationDate = DateTime.UtcNow
                    };

                    await userAddressRepository.CreateUserAddressAsync(newAddress, cancellationToken);
                }
            }
        }
        
        await userRepository.UpdateUserAsync(user, cancellationToken);
        
        var model = mapper.Map<UserDto>(user);
        
        var response = new UpdateUserResponse
        {
            Message = "User have been successfully updated.",
            Item = model
        };
        
        return response;
    }
}