﻿using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUser;

public class GetUserRequest : IRequest<GetUserResponse>
{
    public Guid Id { get; init; }
}