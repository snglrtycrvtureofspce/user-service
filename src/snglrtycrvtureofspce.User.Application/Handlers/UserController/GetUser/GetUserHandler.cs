﻿using AutoMapper;
using MediatR;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Domain.Errors;
using snglrtycrvtureofspce.User.Domain.Interfaces;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUser;

public class GetUserHandler(IUserRepository repository, IMapperBase mapper) : IRequestHandler<GetUserRequest, 
    GetUserResponse>
{
    public async Task<GetUserResponse> Handle(GetUserRequest request, CancellationToken cancellationToken)
    {
        var user = await repository.GetUserAsync(request.Id) 
                   ?? throw new NotFoundException(UserErrors.NotFound(request.Id));
        
        var model = mapper.Map<UserDto>(user);
        
        var response = new GetUserResponse
        {
            Message = "User have been successfully received.",
            Item = model
        };
        
        return response;
    }
}