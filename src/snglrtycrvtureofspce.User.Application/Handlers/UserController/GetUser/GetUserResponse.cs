﻿using snglrtycrvtureofspce.Core.Base.Responses;
using snglrtycrvtureofspce.User.Application.DTOs;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUser;

public class GetUserResponse : ItemResponse<UserDto>;