﻿using System.IdentityModel.Tokens.Jwt;
using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserIdByAccessToken;

public class GetUserIdByAccessTokenHandler : IRequestHandler<GetUserIdByAccessTokenRequest, string>
{
    public Task<string> Handle(GetUserIdByAccessTokenRequest request, CancellationToken cancellationToken)
    {
        var jwtHandler = new JwtSecurityTokenHandler();
        var decodedToken = jwtHandler.ReadJwtToken(request.AccessToken);
        if (decodedToken.Payload.TryGetValue("UserId", out var userInformationIdObj))
        {
            if (Guid.TryParse(userInformationIdObj.ToString(), out var userInformationId))
            {
                return Task.FromResult(userInformationId.ToString());
            }
        }
        
        throw new ArgumentException("UserId not found in the token.");
    }
}