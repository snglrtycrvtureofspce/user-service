﻿using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserIdByAccessToken;

public class GetUserIdByAccessTokenRequest : IRequest<string>
{
    public string AccessToken { get; init; }
}