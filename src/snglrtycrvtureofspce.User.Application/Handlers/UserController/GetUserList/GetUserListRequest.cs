using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserList;

public class GetUserListRequest : IRequest<GetUserListResponse>;