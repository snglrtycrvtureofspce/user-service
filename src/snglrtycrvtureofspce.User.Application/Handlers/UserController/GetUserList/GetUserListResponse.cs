using snglrtycrvtureofspce.Core.Base.Responses;
using snglrtycrvtureofspce.User.Application.DTOs;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserList;

public class GetUserListResponse : PageViewResponse<UserDto>;