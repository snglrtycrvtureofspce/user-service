using AutoMapper;
using MediatR;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Domain.Interfaces;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserList;

public class GetUserListHandler(IUserRepository repository, IMapperBase mapper) : IRequestHandler<GetUserListRequest, 
    GetUserListResponse>
{
    public async Task<GetUserListResponse> Handle(GetUserListRequest request, CancellationToken cancellationToken)
    {
        var users = await repository.GetUserListAsync();
        
        var models = users.Select(mapper.Map<UserDto>).ToList();
        
        var response = new GetUserListResponse
        {
            Message = "User list have been successfully received.",
            Total = models.Count,
            Elements = models
        };
        
        return response;
    }
}