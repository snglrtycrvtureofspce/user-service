﻿using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeUser;

public class RevokeUserRequest : IRequest<RevokeUserResponse>
{
    public string Username { get; set; }
}