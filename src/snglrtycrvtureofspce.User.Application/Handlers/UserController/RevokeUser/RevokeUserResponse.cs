﻿using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeUser;

public class RevokeUserResponse : ItemResponse<string>;