﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Errors;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeUser;

public class RevokeUserHandler(UserManager<UserEntity> userManager) : IRequestHandler<RevokeUserRequest, 
    RevokeUserResponse>
{
    public async Task<RevokeUserResponse> Handle(RevokeUserRequest request, CancellationToken cancellationToken)
    {
        var user = await userManager.FindByNameAsync(request.Username) 
                   ?? throw new NotFoundException(UserErrors.NotFoundByUsername(request.Username));
        
        user.RefreshToken = null;
        await userManager.UpdateAsync(user);
        
        var response = new RevokeUserResponse
        {
            Message = "User have been successfully revoked.",
            Item = user.UserName
        };
        
        return response;
    }
}