﻿using snglrtycrvtureofspce.Core.Base.Responses;
using snglrtycrvtureofspce.User.Application.DTOs;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.AddPhoneNumber;

public class AddPhoneNumberResponse : ItemResponse<UserDto>;