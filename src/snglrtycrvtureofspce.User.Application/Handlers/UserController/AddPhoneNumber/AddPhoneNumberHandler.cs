﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Errors;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.AddPhoneNumber;

public class AddPhoneNumberHandler(UserManager<UserEntity> userManager, IMapperBase mapper) 
    : IRequestHandler<AddPhoneNumberRequest, AddPhoneNumberResponse>
{
    public async Task<AddPhoneNumberResponse> Handle(AddPhoneNumberRequest request, CancellationToken cancellationToken)
    {
        var user = await userManager.FindByIdAsync(request.UserId.ToString()) 
                   ?? throw new NotFoundException(UserErrors.NotFound(request.UserId));

        user.PhoneNumber = request.PhoneNumber;
        user.PhoneNumberConfirmed = false;
        await userManager.UpdateAsync(user);

        // Logic for sending a confirmation code (for the future)

        var model = mapper.Map<UserDto>(user);

        var response = new AddPhoneNumberResponse
        {
            Message = "The user's phone number was successfully added.",
            Item = model
        };

        return response;
    }
}