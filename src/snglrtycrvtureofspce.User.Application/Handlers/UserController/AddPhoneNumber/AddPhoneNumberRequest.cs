﻿using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.AddPhoneNumber;

public class AddPhoneNumberRequest : IRequest<AddPhoneNumberResponse>
{
    public Guid UserId { get; set; }
    
    public string PhoneNumber { get; set; }
}