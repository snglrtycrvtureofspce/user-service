using MediatR;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserRole;

public class GetUserRoleRequest : IRequest<GetUserRoleResponse>
{
    public Guid Id { get; init; }
}