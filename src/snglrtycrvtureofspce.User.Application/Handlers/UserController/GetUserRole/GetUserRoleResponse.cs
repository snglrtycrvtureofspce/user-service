using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserRole;

public class GetUserRoleResponse : ItemResponse<string>;