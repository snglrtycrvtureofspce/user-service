using MediatR;
using Microsoft.AspNetCore.Identity;
using snglrtycrvtureofspce.Core.Exceptions;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Errors;
using snglrtycrvtureofspce.User.Domain.Interfaces;

namespace snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserRole;

public class GetUserRoleHandler(IUserRepository repository, UserManager<UserEntity> userManager) : 
    IRequestHandler<GetUserRoleRequest, GetUserRoleResponse>
{
    public async Task<GetUserRoleResponse> Handle(GetUserRoleRequest request, CancellationToken cancellationToken)
    {
        var user = await repository.GetUserAsync(request.Id) 
                   ?? throw new NotFoundException(UserErrors.NotFound(request.Id));
        
        var userRoles = await userManager.GetRolesAsync(user);
        
        var response = new GetUserRoleResponse
        {
            Message = "User role have been successfully received.",
            Item = string.Join(", ", userRoles)
        };
        
        return response;
    }
}