﻿using AutoMapper;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Application.AutomapperProfiles;

public class UserAddressProfile : Profile
{
    public UserAddressProfile()
    {
        CreateMap<UserAddressEntity, UserAddressDto>();
    }
}