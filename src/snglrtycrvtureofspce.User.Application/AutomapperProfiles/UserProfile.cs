﻿using AutoMapper;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Application.AutomapperProfiles;

public class UserProfile : Profile
{
    public UserProfile()
    {
        CreateMap<UserEntity, UserDto>().MaxDepth(1);
    }
}