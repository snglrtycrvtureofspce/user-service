﻿using AutoMapper;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Application.AutomapperProfiles;

public class UserSocialProfile : Profile
{
    public UserSocialProfile()
    {
        CreateMap<UserSocialEntity, UserSocialDto>();
    }
}