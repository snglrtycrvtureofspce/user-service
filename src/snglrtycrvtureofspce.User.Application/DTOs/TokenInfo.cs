﻿namespace snglrtycrvtureofspce.User.Application.DTOs;

/// <summary>
/// Standard token response of Create, Update, Get single operation
/// </summary>
public class TokenInfo
{
    /// <summary>Token</summary>
    public string AccessToken { get; set; }
    
    /// <summary>Refresh token</summary>
    public string RefreshToken { get; set; }
}