﻿using Microsoft.AspNetCore.Identity;

namespace snglrtycrvtureofspce.User.Application.DTOs;

public class UserDto : IdentityUser<Guid>
{
    public string FirstName { get; set; }
    
    public string LastName { get; set; }
    
    public string? MiddleName { get; set; }
    
    public DateTime? DateOfBirth { get; set; }
    
    public string? Country { get; set; }
    
    public string? City { get; set; }
    
    public string? Language { get; set; }
    
    public string? UserPhoto { get; set; }
    
    public bool Agreement { get; set; }
    
    public bool? ChangePasswordNotification { get; set; }
    
    public string? DeliveryMethod { get; set; }
    
    public string? ClientType { get; set; }
    
    public Guid? UserSocialId { get; set; }
    
    public UserSocialDto UserSocial { get; set; }
    
    public List<UserAddressDto> UserAddresses { get; init; }
}