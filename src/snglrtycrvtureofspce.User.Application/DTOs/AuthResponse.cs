﻿namespace snglrtycrvtureofspce.User.Application.DTOs;

/// <summary>
/// Standard authentication response of Create, Update, Get single operation
/// </summary>
public class AuthResponse
{
    /// <summary>Username</summary>
    public string Username { get; set; }
    
    /// <summary>Email</summary>
    public string Email { get; set; }
    
    /// <summary>User id</summary>
    public Guid UserId { get; set; }
    
    /// <summary>Token</summary>
    public string AccessToken { get; set; }
    
    /// <summary>Refresh token</summary>
    public string RefreshToken { get; set; }
}