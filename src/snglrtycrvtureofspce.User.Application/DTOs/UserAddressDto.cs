﻿using snglrtycrvtureofspce.Core.Base.Infrastructure;

namespace snglrtycrvtureofspce.User.Application.DTOs;

public class UserAddressDto : IEntity
{
    #region IEntity
    public Guid Id { get; set; }
    
    public DateTime CreatedDate { get; set; }
    
    public DateTime ModificationDate { get; set; }
    #endregion
    
    public string Country { get; set; }
    
    public string City { get; set; }
    
    public string Street1 { get; set; }
    
    public string Street2 { get; set; }
    
    public string State { get; set; }
    
    public string PostalCode { get; set; }
    
    public string? AddressType { get; set; }
    
    public Guid UserId { get; set; }
}