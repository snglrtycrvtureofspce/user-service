﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Application.Extensions;

public static class JwtBearerExtensions
{
    public static List<Claim> CreateClaims(this UserEntity user, IEnumerable<IdentityRole<Guid>> roles)
    {
        var claims = new List<Claim>
        {
            new("UserId", user.Id.ToString()),
            new("Username", user.UserName!),
            new("Email", user.Email!),
            new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new(JwtRegisteredClaimNames.Iat, DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString())
        };
        
        claims.AddRange(roles
            .Where(role => role.Name != null)
            .Select(role => new Claim("role", role.Name!)));
        
        return claims;
    }

    private static SigningCredentials CreateSigningCredentials()
    {
        var secret = Environment.GetEnvironmentVariable("KEY");
        if (string.IsNullOrEmpty(secret))
        {
            throw new InvalidOperationException("Key is missing or empty in configuration.");
        }
        
        return new SigningCredentials(
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)), SecurityAlgorithms.HmacSha256);
    }

    public static JwtSecurityToken CreateJwtToken(this IEnumerable<Claim> claims, IConfiguration configuration) => 
        new JwtSecurityToken(
            configuration["Jwt:Issuer"], 
            configuration["Jwt:Audience"],
            claims, 
            expires: DateTime.UtcNow.AddMinutes(configuration.GetSection("Jwt:Expire").Get<int>()), 
            signingCredentials: CreateSigningCredentials()
            );
    
    public static JwtSecurityToken CreateToken(this IConfiguration configuration, IEnumerable<Claim> authClaims)
    {
        var authSigningKey = 
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Environment.GetEnvironmentVariable("KEY")!));
        var tokenValidityInMinutes = configuration.GetSection("Jwt:TokenValidityInMinutes").Get<int>();
        
        var token = new JwtSecurityToken(
            issuer: configuration["Jwt:Issuer"],
            audience: configuration["Jwt:Audience"],
            expires: DateTime.UtcNow.AddMinutes(tokenValidityInMinutes),
            claims: authClaims,
            signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
        );

        return token;
    }

    public static string GenerateRefreshToken()
    {
        var randomNumber = new byte[64];
        using var rng = RandomNumberGenerator.Create();
        rng.GetBytes(randomNumber);
        
        return Convert.ToBase64String(randomNumber);
    }

    public static ClaimsPrincipal? GetPrincipalFromExpiredToken(string? token)
    {
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateAudience = false,
            ValidateIssuer = false,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes
                (Environment.GetEnvironmentVariable("Key")!)),
            ValidateLifetime = false
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var securityToken);
        if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg
                .Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase)) 
            throw new SecurityTokenException("Invalid token");

        return principal;
    }
}