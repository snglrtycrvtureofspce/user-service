﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using snglrtycrvtureofspce.Core.Microservices.Core.JwtAuth.Policies;
using snglrtycrvtureofspce.User.Api.Filters;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.AuthenticateUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.DeleteUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserIdByAccessToken;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserList;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserRole;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.RefreshToken;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.RegisterUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeAllUsers;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.UpdateUser;
using Swashbuckle.AspNetCore.Annotations;

namespace snglrtycrvtureofspce.User.Api.Controllers;

[ApiController]
[Route(template: "[controller]")]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Produces(contentType: "application/json")]
public class UserController(ISender sender) : ControllerBase
{
    /// <summary>
    /// The method provider possibility to get a user identifier by access token.
    /// </summary>
    /// <param name="accessToken">The access token of the user.</param>
    /// <returns>The unique identifier of the user.</returns>
    /// <response code="200">Returns the user identifier.</response>
    /// <response code="404">If the access token is not found.</response>
    [HttpPost(template: "GetUserIdByAccessToken", Name = "GetUserIdByAccessToken")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(string))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    [AllowAnonymous]
    public async Task<IActionResult> GetUserIdByAccessToken([FromBody] string accessToken) => 
        Ok(await sender.Send(new GetUserIdByAccessTokenRequest { AccessToken = accessToken }));
    
    /// <summary>
    /// The method provider possibility to authenticate a user by email and password.
    /// </summary>
    /// <param name="request">The request object containing the user's credentials.</param>
    /// <returns>A token for the authenticated user.</returns>
    /// <response code="200">Returns the authentication token.</response>
    /// <response code="400">If the password is invalid.</response>
    /// <response code="404">If the user is not found.</response>
    [HttpPost(template: "AuthenticateUser", Name = "AuthenticateUser")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(AuthenticateUserResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status400BadRequest)]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    [AllowAnonymous]
    public async Task<ActionResult> AuthenticateUser([FromBody] AuthenticateUserRequest request) => 
        Ok(await sender.Send(request));
    
    /// <summary>
    /// The method provider possibility to register a user.
    /// </summary>
    /// <param name="request">The request object containing the new user's details.</param>
    /// <returns>The authentication token for the newly registered user.</returns>
    /// <response code="200">Returns the authentication token for the new user.</response>
    /// <response code="400">If the registration data is invalid.</response>
    [HttpPost(template: "RegisterUser", Name = "RegisterUser")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(AuthenticateUserResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest)]
    [AllowAnonymous]
    public async Task<ActionResult> RegisterUser([FromBody] RegisterUserRequest request) => 
        Ok(await sender.Send(request));
    
    /*/// <summary>
    /// The method provides the possibility to add a phone number for a user.
    /// </summary>
    /// <returns></returns>
    [HttpPost("AddPhoneNumber", Name = "AddPhoneNumber")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK)]
    public async Task<IActionResult> AddPhoneNumber([FromBody] AddPhoneNumberRequest request)
    {
        request.UserId = User.Claims.GetUserId();
        return Ok(await sender.Send(request));
    }

    /// <summary>
    /// The method provides the possibility to confirm a phone number for a user.
    /// </summary>
    /// <returns></returns>
    [HttpPost("ConfirmPhoneNumber", Name = "ConfirmPhoneNumber")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK)]
    public async Task<IActionResult> ConfirmPhoneNumber([FromBody] ConfirmPhoneNumberRequest request)
    {
        request.UserId = User.Claims.GetUserId();
        return Ok(await sender.Send(request));
    }*/
    
    /// <summary>
    /// The method provider possibility to refresh a token by access token and refresh token.
    /// </summary>
    /// <param name="request">The request object containing the tokens to be refreshed.</param>
    /// <returns>A new access token.</returns>
    /// <response code="200">Returns the new access token.</response>
    /// <response code="400">If the tokens are invalid.</response>
    [HttpPost(template: "RefreshToken", Name = "RefreshToken")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(RefreshTokenResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status400BadRequest)]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    [AllowAnonymous]
    public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest request) => 
        Ok(await sender.Send(request));

    /// <summary>
    /// The method provider possibility to revoke a user by bearer token (administrators only).
    /// </summary>
    /// <param name="request">The request object containing the user details to be revoked.</param>
    /// <returns>A confirmation of the revocation.</returns>
    /// <response code="200">Returns a confirmation of the revoked user.</response>
    /// <response code="400">If the caller does not have permission to revoke the user.</response>
    [HttpPost(template: "RevokeUser", Name = "RevokeUser")]
    [HasAccessAuthorization]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(RevokeUserResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> RevokeUser([FromBody] RevokeUserRequest request) => Ok(await sender.Send(request));
    
    /// <summary>
    /// The method provider possibility to revoke all users by bearer token (administrators only).
    /// </summary>
    /// <returns>A confirmation of the revocation.</returns>
    /// <response code="200">Returns a confirmation of all revoked users.</response>
    /// <response code="400">If the caller does not have permission to revoke users.</response>
    [HttpPost(template: "RevokeAllUsers", Name = "RevokeAllUsers")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(RevokeAllUsersResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status400BadRequest)]
    [Authorize(Policy = Policies.AdminOnly)]
    public async Task<IActionResult> RevokeAllUsers() => Ok(await sender.Send(new RevokeAllUsersRequest()));
    
    /// <summary>
    /// The method provider possibility to get an all users.
    /// </summary>
    /// <returns>A list of users.</returns>
    /// <response code="200">Returns a list of all users.</response>
    [HttpGet(Name = "GetUserList")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetUserListResponse))]
    [Authorize(Policy = Policies.AdminOnly)]
    public async Task<IActionResult> GetUserList() => Ok(await sender.Send(new GetUserListRequest()));
    
    /// <summary>
    /// The method provider possibility to get a user by identifier.
    /// </summary>
    /// <param name="id">The unique identifier of the user.</param>
    /// <returns>The user item corresponding to the provided identifier.</returns>
    /// <response code="200">Returns the user item.</response>
    /// <response code="404">If the user is not found.</response>
    [HttpGet(template: "{id:guid}", Name = "GetUser")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetUserResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    [AllowAnonymous]
    public async Task<IActionResult> GetUser(Guid id) => Ok(await sender.Send(new GetUserRequest { Id = id }));
    
    /// <summary>
    /// The method provider possibility to get a user role by identifier.
    /// </summary>
    /// <param name="id">The unique identifier of the user.</param>
    /// <returns>The role of the user corresponding to the provided identifier.</returns>
    /// <response code="200">Returns the user role.</response>
    /// <response code="404">If the user is not found.</response>
    [HttpGet(template: "{id:guid}/GetUserRole", Name = "GetUserRole")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetUserRoleResponse))]
    [AllowAnonymous]
    public async Task<IActionResult> GetUserRole(Guid id) => Ok(await sender.Send(new GetUserRoleRequest { Id = id }));
    
    /// <summary>
    /// The method provider possibility to update a user by identifier.
    /// </summary>
    /// <param name="id">Identifier of the user to be received</param>
    /// <param name="request">The request object containing the details of the user to be updated.</param>
    /// <returns>A confirmation of the update.</returns>
    /// <response code="200">Returns a confirmation of the updated user information.</response>
    /// <response code="400">If the request is invalid.</response>
    [HttpPut(template: "{id:guid}", Name = "UpdateUser")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(UpdateUserResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdateUser(Guid id, [FromBody] UpdateUserRequest request)
    {
        request.Id = id;
        return Ok(await sender.Send(request));
    }
    
    /*/// <summary>
    /// The method provider possibility to update a user photo by identifier.
    /// </summary>
    /// <param name="id">Identifier of the user to be received</param>
    /// <param name="request">The request object containing the details of the user to be updated.</param>
    /// <returns>A confirmation of the update.</returns>
    /// <response code="200">Returns a confirmation of the updated user photo information.</response>
    /// <response code="400">If the request is invalid.</response>
    [HttpPut(template: "{id:guid}/UpdateUserPhoto", Name = "UpdateUserPhoto")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(UpdateUserPhotoResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdateUserPhoto(Guid id, [FromForm] UpdateUserPhotoRequest request)
    {
        request.Id = id;
        return Ok(await sender.Send(request));
    }*/
    
    /// <summary>
    /// The method provider possibility to delete a user by user.
    /// </summary>
    /// <param name="id">The unique identifier of the user to be deleted.</param>
    /// <returns>A confirmation of the deletion.</returns>
    /// <response code="200">Returns a confirmation of the deleted user.</response>
    /// <response code="404">If the user is not found.</response>
    [HttpDelete(template: "{id:guid}", Name = "DeleteUser")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(DeleteUserResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    [Authorize(Policy = Policies.AdminOnly)]
    public async Task<IActionResult> DeleteUser(Guid id) => Ok(await sender.Send(new DeleteUserRequest { Id = id }));
}