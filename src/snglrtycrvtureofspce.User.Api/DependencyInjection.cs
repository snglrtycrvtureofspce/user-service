﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using snglrtycrvtureofspce.Core.Microservices.Core.Configurations;
using snglrtycrvtureofspce.Core.Microservices.Core.ServerMiddleware;
using snglrtycrvtureofspce.User.Api.Configurations;

namespace snglrtycrvtureofspce.User.Api;

public static class DependencyInjection
{
    public static IServiceCollection AddApiServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddServerControllers();
        services.AddCustomAuthorization();
        
        services.ConfigureApiVersioning();
        services.ConfigureSwagger(configuration);
        
        services.AddCors(options =>
        {
            options.AddPolicy("AllowAll", corsPolicyBuilder =>
                corsPolicyBuilder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
        });
        
        return services;
    }
}