﻿using FluentValidation.Results;

namespace snglrtycrvtureofspce.User.Domain.Errors; 

public static class AuthorizationError
{
    public static IEnumerable<ValidationFailure> UnableToGetAuthorizationToken() => new List<ValidationFailure>
        { new("AuthorizationToken", "Unable to get authorization token") };
}