﻿using FluentValidation.Results;

namespace snglrtycrvtureofspce.User.Domain.Errors;

public static class TokenErrors
{
    public static IEnumerable<ValidationFailure> InvalidAccessToken() => 
        new List<ValidationFailure> { new(nameof(Nullable), "Invalid access token.") };
    
    public static IEnumerable<ValidationFailure> InvalidRefreshToken() => 
        new List<ValidationFailure> { new(nameof(Nullable), "Invalid refresh token.") };
}