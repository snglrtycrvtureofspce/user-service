﻿using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Domain.Interfaces;

public interface IUserAddressRepository
{
    Task CreateUserAddressAsync(UserAddressEntity userAddress, CancellationToken cancellationToken);
    
    Task<UserAddressEntity> GetUserAddressByTypeAsync(Guid userId, string addressType);
    
    Task UpdateUserAddressAsync(UserAddressEntity userAddress, CancellationToken cancellationToken);
}