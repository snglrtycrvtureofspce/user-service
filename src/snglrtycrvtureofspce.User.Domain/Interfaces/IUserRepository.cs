﻿using Microsoft.AspNetCore.Identity;
using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Domain.Interfaces;

public interface IUserRepository
{
    Task<UserEntity> GetUserAsync(Guid id);
    
    Task<UserEntity> GetUserByUserNameAsync(string userName);
    
    Task<UserEntity> GetUserByEmailAsync(string email);
    
    Task<IEnumerable<Guid>> GetUserRolesAsync(Guid userId);
    
    Task<IEnumerable<IdentityRole<Guid>>> GetRolesByIdsAsync(IEnumerable<Guid> roleIds);
    
    Task<IEnumerable<UserEntity>> GetUserListAsync();
    
    Task CleanExpiredRefreshTokensAsync(CancellationToken cancellationToken);
    
    Task UpdateUserAsync(UserEntity user, CancellationToken cancellationToken);
    
    Task DeleteUserAsync(UserEntity user, CancellationToken cancellationToken);
}