﻿using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Domain.Interfaces;

public interface IUserSocialRepository
{
    Task CreateUserSocialAsync(UserSocialEntity userSocial, CancellationToken cancellationToken);
    
    Task<UserSocialEntity> GetUserSocialAsync(Guid? id);
    
    Task UpdateUserSocialAsync(UserSocialEntity userSocial, CancellationToken cancellationToken);
}