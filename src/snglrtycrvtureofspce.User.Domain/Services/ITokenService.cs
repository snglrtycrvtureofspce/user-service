﻿using Microsoft.AspNetCore.Identity;
using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Domain.Services;

public interface ITokenService
{
    string CreateToken(UserEntity user, IEnumerable<IdentityRole<Guid>> role);
}