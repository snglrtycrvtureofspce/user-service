﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Core.Base.Infrastructure;
using snglrtycrvtureofspce.User.Domain.Entities;

namespace snglrtycrvtureofspce.User.Infrastructure.Data;

public class UserDbContext : IdentityDbContext<UserEntity, IdentityRole<Guid>, Guid>
{
    public virtual DbSet<UserSocialEntity> UserSocials { get; init; }
    
    public virtual DbSet<UserAddressEntity> UserAddresses { get; init; }
    
    public UserDbContext (DbContextOptions<UserDbContext> options) : base(options) { Database.Migrate(); }
    
    public UserDbContext() { }
    
    public override int SaveChanges()
    {
        ChangeTracker.DetectChanges();
        
        var added = ChangeTracker
            .Entries()
            .Where(w => w.State == EntityState.Added)
            .Select(s => s.Entity)
            .ToList();
        
        foreach (var entry in added)
        {
            if (entry is not IEntity entity)
            {
                continue;
            }

            entity.CreatedDate = DateTime.UtcNow;
            entity.ModificationDate = DateTime.UtcNow;
        }
        
        var updated = ChangeTracker
            .Entries()
            .Where(w => w.State == EntityState.Modified)
            .Select(s => s.Entity)
            .ToList();
        
        foreach (var entry in updated)
        {
            if (entry is IEntity entity)
            {
                entity.ModificationDate = DateTime.UtcNow;
            }
        }
        
        return base.SaveChanges();
    }
    
    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
    {
        return Task.Run(SaveChanges, cancellationToken);
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<UserEntity>(e =>
        {
            e.Property(x => x.RefreshToken)
                .HasMaxLength(255);
            
            e.Property(x => x.FirstName)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.LastName)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.MiddleName)
                .HasMaxLength(255);
            
            e.Property(x => x.Country)
                .HasMaxLength(255);
            
            e.Property(x => x.City)
                .HasMaxLength(255);
            
            e.Property(x => x.Language)
                .HasMaxLength(255);
            
            e.Property(x => x.UserPhoto)
                .HasMaxLength(255);
            
            e.Property(x => x.DeliveryMethod)
                .HasMaxLength(255);
            
            e.Property(x => x.ClientType)
                .HasMaxLength(255);
            
            e.Property(x => x.UserName)
                .HasMaxLength(5000)
                .IsRequired();
            
            e.HasOne(u => u.UserSocial)
                .WithOne()
                .HasForeignKey<UserEntity>(u => u.UserSocialId)
                .OnDelete(DeleteBehavior.Cascade);
        });
        
        modelBuilder.Entity<UserSocialEntity>(e =>
        {
            e.Property(x => x.Site)
                .HasMaxLength(255);
            
            e.Property(x => x.FacebookLink)
                .HasMaxLength(255);
            
            e.Property(x => x.TwitterLink)
                .HasMaxLength(255);
            
            e.Property(x => x.InstagramLink)
                .HasMaxLength(255);
            
            e.Property(x => x.SkypeLink)
                .HasMaxLength(255);
            
            e.Property(x => x.LinkedIn)
                .HasMaxLength(255);
            
            e.Property(x => x.VkLink)
                .HasMaxLength(255);
        });
        
        modelBuilder.Entity<UserAddressEntity>(e =>
        {
            e.Property(x => x.Country)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.City)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Street1)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Street2)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.State)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.PostalCode)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.AddressType)
                .HasMaxLength(255);
            
            e.HasOne(ua => ua.User)
                .WithMany(u => u.UserAddresses)
                .HasForeignKey(ua => ua.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        });
        
        base.OnModelCreating(modelBuilder);
    }
}