﻿using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using snglrtycrvtureofspce.User.Application.Extensions;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Services;

namespace snglrtycrvtureofspce.User.Infrastructure.Services;

public class TokenService(IConfiguration configuration) : ITokenService
{
    public string CreateToken(UserEntity user, IEnumerable<IdentityRole<Guid>> roles) => 
        new JwtSecurityTokenHandler().WriteToken(user.CreateClaims(roles).CreateJwtToken(configuration));
}