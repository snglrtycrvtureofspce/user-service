﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Interfaces;
using snglrtycrvtureofspce.User.Infrastructure.Data;

namespace snglrtycrvtureofspce.User.Infrastructure.Implementations;

public class UserRepository(UserDbContext context) : IUserRepository
{
    public async Task<UserEntity> GetUserAsync(Guid id) => await context.Users.FindAsync(id);
    
    public async Task<UserEntity> GetUserByUserNameAsync(string normalizedUserName) => 
        await context.Users.FirstOrDefaultAsync(u => u.NormalizedUserName == normalizedUserName);
    
    public async Task<UserEntity> GetUserByEmailAsync(string email) => 
        await context.Users.FirstOrDefaultAsync(u => u.Email == email);
    
    public async Task<IEnumerable<UserEntity>> GetUserListAsync() => await context.Users.AsNoTracking().ToListAsync();
    
    public async Task<IEnumerable<Guid>> GetUserRolesAsync(Guid userId) => 
        await context.UserRoles.Where(r => r.UserId == userId).Select(r => r.RoleId).ToListAsync();
    
    public async Task<IEnumerable<IdentityRole<Guid>>> GetRolesByIdsAsync(IEnumerable<Guid> roleIds) => 
        await context.Roles.Where(r => roleIds.Contains(r.Id)).ToListAsync();
    
    public async Task CleanExpiredRefreshTokensAsync(CancellationToken cancellationToken)
    {
        var expiredUsers = await context.Users
            .Where(user => user.RefreshTokenExpiryTime <= DateTime.UtcNow)
            .ToListAsync(cancellationToken);

        foreach (var user in expiredUsers)
        {
            user.RefreshToken = null;
            user.RefreshTokenExpiryTime = DateTime.MinValue;
        }

        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task UpdateUserAsync(UserEntity user, CancellationToken cancellationToken)
    {
        context.Users.Update(user);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task DeleteUserAsync(UserEntity user, CancellationToken cancellationToken)
    {
        context.Users.Remove(user);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}