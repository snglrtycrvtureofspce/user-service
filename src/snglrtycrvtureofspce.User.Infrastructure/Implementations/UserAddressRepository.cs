﻿using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Interfaces;
using snglrtycrvtureofspce.User.Infrastructure.Data;

namespace snglrtycrvtureofspce.User.Infrastructure.Implementations;

public class UserAddressRepository(UserDbContext context) : IUserAddressRepository
{
    public async Task CreateUserAddressAsync(UserAddressEntity userAddress, CancellationToken cancellationToken)
    {
        await context.UserAddresses.AddAsync(userAddress, cancellationToken);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task<UserAddressEntity> GetUserAddressByTypeAsync(Guid userId, string addressType) =>
        await context.UserAddresses.FirstOrDefaultAsync(a => a.UserId == userId && a.AddressType == addressType);
    
    public async Task UpdateUserAddressAsync(UserAddressEntity userAddress, CancellationToken cancellationToken)
    {
        context.UserAddresses.Update(userAddress);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}