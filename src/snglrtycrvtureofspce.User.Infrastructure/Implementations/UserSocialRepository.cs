﻿using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Interfaces;
using snglrtycrvtureofspce.User.Infrastructure.Data;

namespace snglrtycrvtureofspce.User.Infrastructure.Implementations;

public class UserSocialRepository(UserDbContext context) : IUserSocialRepository
{
    public async Task CreateUserSocialAsync(UserSocialEntity userSocial, CancellationToken cancellationToken)
    {
        await context.UserSocials.AddAsync(userSocial, cancellationToken);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task<UserSocialEntity> GetUserSocialAsync(Guid? id) => await context.UserSocials.FindAsync(id);
    
    public async Task UpdateUserSocialAsync(UserSocialEntity userSocial, CancellationToken cancellationToken)
    {
        context.UserSocials.Update(userSocial);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}