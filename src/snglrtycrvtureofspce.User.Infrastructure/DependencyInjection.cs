﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using snglrtycrvtureofspce.Api.File;
using snglrtycrvtureofspce.Core.Microservices.Core.JwtAuth;
using snglrtycrvtureofspce.Core.Microservices.Core.ServerMiddleware;
using snglrtycrvtureofspce.User.Domain.Entities;
using snglrtycrvtureofspce.User.Domain.Interfaces;
using snglrtycrvtureofspce.User.Domain.Services;
using snglrtycrvtureofspce.User.Infrastructure.Data;
using snglrtycrvtureofspce.User.Infrastructure.Implementations;
using snglrtycrvtureofspce.User.Infrastructure.Jobs;
using snglrtycrvtureofspce.User.Infrastructure.Services;

namespace snglrtycrvtureofspce.User.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        var connectionString = Environment.GetEnvironmentVariable("DEPLOYCONNECTION");
        services.AddDbContext<UserDbContext>(options =>
        {
            if (connectionString != null) options.UseNpgsql(connectionString);
        });
        
        services.AddIdentity<UserEntity, IdentityRole<Guid>>()
            .AddEntityFrameworkStores<UserDbContext>()
            .AddUserManager<UserManager<UserEntity>>()
            .AddSignInManager<SignInManager<UserEntity>>();
        
        services.AddTransient(_ => new RefitSettings
        {
            ContentSerializer = new NewtonsoftJsonContentSerializer(new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }),
            CollectionFormat = CollectionFormat.Multi
        });
        
        services.AddTransient(sc =>
            RestService.For<ISystemApi>(
                new JwtHttpClient(configuration.GetMicroserviceHost("FileApi"),
                    sc.GetService<IHttpContextAccessor>() 
                    ?? throw new InvalidOperationException()), sc.GetService<RefitSettings>()));
        
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IUserSocialRepository, UserSocialRepository>();
        services.AddScoped<IUserAddressRepository, UserAddressRepository>();
        services.AddScoped<ITokenService, TokenService>();
        services.AddScoped<TokenCleanupJob>();
        
        return services;
    }
}