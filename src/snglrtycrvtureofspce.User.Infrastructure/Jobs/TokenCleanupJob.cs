﻿using Microsoft.Extensions.Logging;
using snglrtycrvtureofspce.User.Domain.Interfaces;

namespace snglrtycrvtureofspce.User.Infrastructure.Jobs;

public class TokenCleanupJob(IUserRepository userRepository, ILogger<TokenCleanupJob> logger)
{
    public async Task CleanExpiredRefreshTokensAsync(CancellationToken cancellationToken)
    {
        try
        {
            await userRepository.CleanExpiredRefreshTokensAsync(cancellationToken);
            logger.LogInformation("Clearing of expired refresh tokens is completed.");
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Error when clearing expired refresh tokens.");
        }
    }
}