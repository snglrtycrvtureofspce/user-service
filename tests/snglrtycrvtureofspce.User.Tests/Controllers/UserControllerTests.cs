﻿using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using snglrtycrvtureofspce.User.Api.Controllers;
using snglrtycrvtureofspce.User.Application.DTOs;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.AuthenticateUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.DeleteUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserIdByAccessToken;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserList;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.GetUserRole;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.RefreshToken;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.RegisterUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeAllUsers;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.RevokeUser;
using snglrtycrvtureofspce.User.Application.Handlers.UserController.UpdateUser;

namespace snglrtycrvtureofspce.User.Tests.Controllers;

public class UserControllerTests
{
    private readonly Mock<ISender> _senderMock;
    private readonly UserController _controller;

    public UserControllerTests()
    {
        _senderMock = new Mock<ISender>();
        _controller = new UserController(_senderMock.Object);
    }

    [Fact]
    public async Task GetUserIdByAccessToken_ReturnsOkResult()
    {
        const string token = "test-access-token";
        const string userId = "user-id";
        _senderMock.Setup(x => x.Send(It.IsAny<GetUserIdByAccessTokenRequest>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(userId);
        
        var result = await _controller.GetUserIdByAccessToken(token);
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(userId);
    }

    [Fact]
    public async Task AuthenticateUser_ReturnsOkResult()
    {
        var request = new AuthenticateUserRequest { Email = "test@example.com", Password = "password" };
        
        var authResponse = new AuthResponse
        {
            AccessToken = "test-access-token", 
            RefreshToken = "test-refresh-token"
        };
        var response = new AuthenticateUserResponse
        {
            Message = "Authentication have been successful.",
            Item = authResponse
        };

        _senderMock.Setup(x => x.Send(request, It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.AuthenticateUser(request);
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }

    [Fact]
    public async Task RegisterUser_ReturnsOkResult()
    {
        var request = new RegisterUserRequest { Email = "test@example.com", Password = "password" };
        var response = new AuthenticateUserResponse();
        _senderMock.Setup(x => x.Send(request, It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.RegisterUser(request);
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }

    [Fact]
    public async Task RefreshToken_ReturnsOkResult()
    {
        var request = new RefreshTokenRequest { AccessToken = "access-token", RefreshToken = "refresh-token" };
        var response = new RefreshTokenResponse();
        _senderMock.Setup(x => x.Send(request, It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.RefreshToken(request);
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }

    [Fact]
    public async Task RevokeUser_ReturnsOkResult()
    {
        var request = new RevokeUserRequest { Username = "username" };
        var response = new RevokeUserResponse();
        _senderMock.Setup(x => x.Send(request, It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.RevokeUser(request);
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }

    [Fact]
    public async Task RevokeAllUsers_ReturnsOkResult()
    {
        var response = new RevokeAllUsersResponse();
        _senderMock.Setup(x => x.Send(It.IsAny<RevokeAllUsersRequest>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.RevokeAllUsers();
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }

    [Fact]
    public async Task GetAllUsers_ReturnsOkResult()
    {
        var response = new GetUserListResponse();
    
        _senderMock.Setup(x => x.Send(It.IsAny<GetUserListRequest>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.GetUserList();
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }

    [Fact]
    public async Task GetUser_ReturnsOkResult()
    {
        var userId = Guid.NewGuid();
        var response = new GetUserResponse();
        _senderMock.Setup(x => x.Send(It.IsAny<GetUserRequest>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.GetUser(userId);
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }

    [Fact]
    public async Task GetUserRole_ReturnsOkResult()
    {
        var userId = Guid.NewGuid();
        var response = new GetUserRoleResponse();
        _senderMock.Setup(x => x.Send(It.IsAny<GetUserRoleRequest>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.GetUserRole(userId);
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }

    [Fact]
    public async Task UpdateUser_ReturnsOkResult()
    {
        var userId = Guid.NewGuid();
        var request = new UpdateUserRequest { Id = userId, Email = "updated@example.com" };
        var response = new UpdateUserResponse();
        _senderMock.Setup(x => x.Send(request, It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.UpdateUser(userId, request);
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }

    [Fact]
    public async Task DeleteUser_ReturnsOkResult()
    {
        var userId = Guid.NewGuid();
        var response = new DeleteUserResponse();
        _senderMock.Setup(x => x.Send(It.IsAny<DeleteUserRequest>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(response);
        
        var result = await _controller.DeleteUser(userId);
        
        var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
        okResult.Value.Should().Be(response);
    }
}